package com.roananik.lessons.lessons10_1;

import org.junit.jupiter.api.Test;

import static com.roananik.lessons.lessons10_1.Point.distanceBetween;
import static org.junit.jupiter.api.Assertions.*;

public class TestPoints {
    @Test
    public void compareSamePoints() {
        Point first = new Point(2, 2);
        Point second = new Point(2, 2);

        assertEquals(first, second);
    }

    @Test
    public void compareDifferentPoints() {
        Point first = new Point(1, 2);
        Point second = new Point(2, 2);

        assertNotEquals(first, second);
    }

    @Test
    public void equalsTransitiveTest() {
        Point first = new Point(1, 2);
        Point second = new Point(1, 2);
        Point third = new Point(1, 2);

        assertTrue(first.equals(second));
        assertTrue(second.equals(third));
        assertTrue(third.equals(first));


    }

    @Test
    public void equalsSymmetricTest() {
        Point first = new Point(1, 2);
        Point second = new Point(1, 2);

        assertTrue(first.equals(second));
        assertTrue(second.equals(first));
    }

    @Test
    public void equalsTestNull() {
        Point first = new Point(1, 2);
        Point second = null;

        assertNotEquals(first, second);

    }

    @Test
    public void equalsTest() {
        Point point = new Point(1, 1);

        assertEquals(point, point);
    }

    @Test
    public void hashCodeEquals() {
        Point first = new Point(2, 2);
        Point second = new Point(2, 2);

        assertEquals(first.hashCode(), second.hashCode());

    }

    @Test
    public void hashCodeDifferent() {
        Point first = new Point(1, 2);
        Point second = new Point(2, 2);

        assertNotEquals(first.hashCode(), second.hashCode());

    }

    @Test
    public void distanceBetweenNullTest() {
        Point first = new Point(3, 4);
        Point second = null;

        double distance = -1;

        assertEquals(distance, distanceBetween(first, second), 0.01);

    }

    @Test
    public void distanceBetweenTest() {
        Point first = new Point(3, 4);
        Point second = new Point(5, 8);

        double distance = 4.47;

        assertEquals(distance, distanceBetween(first, second), 0.01);

    }


    @Test
    public void distanceToTest() {
        Point first = new Point(3, 4);
        Point second = new Point(5, 8);

        double distance = 4.47;

        assertEquals(distance, first.distanceTo(second), 0.01);

    }

    @Test
    public void distanceToPoint() {
        Point first = new Point(3, 4);

        double distance = 0.0;

        assertEquals(distance, first.distanceTo(first), 0.01);

    }

    @Test
    public void distanceNullPoint() {
        Point first = new Point(0, 0);

        double distance = -1;

        assertEquals(distance, first.distanceTo(null), 0.01);
    }

    @Test
    public void exceptionTest() {
        Point first = null;
        Point second = new Point(5, 8);

        assertThrows(NullPointerException.class, () -> {
            first.distanceTo(second);
        });

    }

    @Test
    public void testGetX() {
        Point second = new Point(5, 8);
        int expectedX = 5;
        assertEquals(expectedX, second.getX());

    }

    @Test
    public void testSetX() {
        Point second = new Point(5, 8);
        int expectedX = 6;
        second.setX(expectedX);
        assertEquals(expectedX, second.getX());


    }

    @Test
    public void testGetY() {
        Point second = new Point(5, 8);
        int expectedY = 8;
        assertEquals(expectedY, second.getY());

    }

    @Test
    public void testSetY() {
        Point second = new Point(5, 8);
        int expectedY = 5;
        second.setY(expectedY);
        assertEquals(expectedY, second.getY());

    }


}
