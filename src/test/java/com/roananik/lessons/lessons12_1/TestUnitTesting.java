package com.roananik.lessons.lessons12_1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestUnitTesting {
    private UnitTesting unitTesting;

    @BeforeEach
    public void unit() {
        unitTesting = new UnitTesting();
    }


    @Test
    public void squareDimensionalArray() {
        int[][] array = new int[4][4];

        assertTrue(unitTesting.squareArray(array));

    }

    @Test
    public void jaggedArray() {
        int[][] array = {{1, 2, 3, 4},
                {1, 2, 3},
                {1, 2, 3}};
        assertFalse(unitTesting.squareArray(array));
    }

    @Test
    public void jaggedDimensionalNullArray() {
        int[][] array = {{1, 2, 3}, null, {4, 5, 6, 7}};
        assertFalse(unitTesting.squareArray(array));
    }

    @Test
    public void jaggedDimensionalArray() {
        int[][] array = {{1, 2}, {1, 2, 3}, {1, 2, 3, 4}};

        assertFalse(unitTesting.squareArray(array));
    }

    @Test
    public void nullDimensionalArray() {
        int[][] array = null;

        assertFalse(unitTesting.squareArray(array));
    }

    @Test
    public void emptyDimensionalArray() {
        int[][] array = {};

        assertFalse(unitTesting.squareArray(array));
    }

    @Test
    public void averageTest() {
        int[] array = {1, 2, 3, 4, 5, 6};
        double result = 3.5;
        assertEquals(result, unitTesting.average(array));
    }

    @Test
    public void emptyArrayTest() {
        int[] array = {};

        assertEquals(-1, unitTesting.average(array));
    }

    @Test
    public void nullArrayTest() {
        int[] array = null;

        assertEquals(-1,unitTesting.average(array));
    }


}
