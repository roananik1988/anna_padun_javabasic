package com.roananik.lessons.lessons11;

import com.roananik.lessons.lessons11.lessonsMain11.FahrenheitCelsius;
import com.roananik.lessons.lessons11.lessonsMain11.KelvinCelsius;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConvertersTest {
    public void kelvin() {
        KelvinCelsius kelvinCelsius = new KelvinCelsius();
        double kelvin = kelvinCelsius.converterFromCelsius(20);
        assertEquals(kelvin, 293.15);
    }

    @Test
    public void fahrenheit() {
        FahrenheitCelsius fahrenheitCelsius = new FahrenheitCelsius();
        double fahrenheit = fahrenheitCelsius.converterFromCelsius(20);
        assertEquals(fahrenheit, 68.0);
    }
}



