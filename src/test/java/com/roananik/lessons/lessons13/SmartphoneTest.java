package com.roananik.lessons.lessons13;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SmartphoneTest {
}


class IphoneTest {
    private Iphone iphone;

    @BeforeEach
    void iphone() {
        iphone = new Iphone();
    }

    @Test
    public void callTestIphone() {
        String phoneNumber = "4915209943429";

        assertEquals(iphone.call(phoneNumber), "4915209943429");

    }

    @Test
    public void smsTestIphone() {
        String sms = "goodbye";

        assertEquals(iphone.sms(sms), "goodbye");
    }

    @Test
    public void internetTestIphone() {
        String url = "https://gitlab.com";

        assertEquals(iphone.internet(url), "https://gitlab.com");

    }
}

class AndroidTest {
    private Android android;

    @BeforeEach
    void android() {
        android = new Android();
    }

    @Test
    public void callTestAndroid() {
        String phoneNumber = "44356798228";

        assertEquals(android.call(phoneNumber), "44356798228");

    }

    @Test
    public void smsTestAndroid() {
        String sms = "\"hello\"";

        assertEquals(android.sms(sms), "\"hello\"");


    }

    @Test
    public void internetTestAndroid() {
        String url = "https://lms.ithillel.ua";

        assertEquals(android.internet(url), "https://lms.ithillel.ua");
    }

}
