package com.roananik.lessons.lessons14_1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertThrows;


class UserTest {
    @Test
    void invalidUserTest() {
        String login = "loginLogin";
        String password = "qwerty.@1";
        String confirmPassword = "qwerty.@1";

        Assertions.assertDoesNotThrow(() -> new User(login, password, confirmPassword));
    }

    @Test
    void loginFalse() {
        String login = "loginлогин";
        String password = "qwerty.@1";
        String confirmPassword = "qwerty.@1";

        assertThrows(WrongLoginException.class, () -> new User(login, password, confirmPassword));


    }

    @Test
    void longLogin() {
        String login = "loginqwertyasdfghzxcvbnm";
        String password = "qwerty.@1";
        String confirmPassword = "qwerty.@1";

        assertThrows(WrongLoginException.class, () -> new User(login, password, confirmPassword));


    }

    @Test
    void loginIsEmpty() {
        String login = "";
        String password = "qwerty.@1";
        String confirmPassword = "qwerty.@1";

        assertThrows(WrongLoginException.class, () -> new User(login, password, confirmPassword));

    }

    @Test
    void loginIsNull() {
        String login = null;
        String password = "qwerty.@1";
        String confirmPassword = "qwerty.@1";

        assertThrows(WrongLoginException.class, () -> new User(login, password, confirmPassword));

    }

    @Test
    void passwordShort() {
        String login = "loginLogin";
        String password = "qw.@1";
        String confirmPassword = "qw.@1";

        assertThrows(WrongPasswordException.class, () -> new User(login, password, confirmPassword));

    }

    @Test
    void passwordLong() {
        String login = "loginLogin";
        String password = "qw.@1ededededededededededededededede";
        String confirmPassword = "qw.@1ededededededededededededededede";

        assertThrows(WrongPasswordException.class, () -> new User(login, password, confirmPassword));

    }

    @Test
    void passwordIsEmpty() {
        String login = "loginLogin";
        String password = "";
        String confirmPassword = "";

        assertThrows(WrongPasswordException.class, () -> new User(login, password, confirmPassword));

    }

    @Test
    void passwordIsNull() {
        String login = "loginLogin";
        String password = null;
        String confirmPassword = null;

        assertThrows(WrongPasswordException.class, () -> new User(login, password, confirmPassword));

    }

    @Test
    void wrongConfirmPassword() {
        String login = "loginLogin";
        String password = "qwerty.1@";
        String confirmPassword = "qwerty.2@";

        assertThrows(WrongPasswordException.class, () -> new User(login, password, confirmPassword));

    }
}