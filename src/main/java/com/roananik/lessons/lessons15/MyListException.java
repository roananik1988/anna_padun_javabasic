package com.roananik.lessons.lessons15;

public class MyListException extends RuntimeException {
    public MyListException(String message) {
        super(message);
    }
}
