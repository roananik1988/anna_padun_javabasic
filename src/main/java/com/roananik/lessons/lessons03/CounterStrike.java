package com.roananik.lessons.lessons03;

import java.util.Scanner;

public class CounterStrike {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter First Team Name");
        String firstTeam = scanner.nextLine();
        System.out.println("Enter points for five players");

        int captain = scanner.nextByte();
        int lieutenant = scanner.nextInt();
        int major = scanner.nextInt();
        int ensign = scanner.nextInt();
        int sergeant = scanner.nextInt();

        System.out.println("Enter Second Team Name");
        scanner.nextLine();
        String secondTeam = scanner.nextLine();
        System.out.println("Enter points for five players");

        int boss = scanner.nextInt();
        int cosnillere = scanner.nextInt();
        int henchman = scanner.nextInt();
        int capo = scanner.nextInt();
        int bandit = scanner.nextInt();


        double average1 = (double) (captain + lieutenant + major + ensign + sergeant) / 5;
        double average2 = (double) (boss + cosnillere + henchman + capo + bandit) / 5;

        double eps = 0.0001;


        if (Math.abs(average1 - average2) < eps) {
            System.out.println("DRAW!!!");
        } else if (average1 < average2) {
            System.out.printf("Team won: %s scored: %.2f points ", secondTeam, average2);
        } else if (average1 > average2) {
            System.out.printf("Team won: %s scored: %.2f points ", firstTeam, average1);
        }
        scanner.close();

    }
}
