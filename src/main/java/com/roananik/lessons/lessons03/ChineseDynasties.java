package com.roananik.lessons.lessons03;

public class ChineseDynasties {
    public static void main(String[] args) {
        int leeQuantity = 860;
        int mingQuantity = (int) (leeQuantity * 1.5);


        int warriorLee = 13;
        int archerLee = 24;
        int riderLee = 46;

        int leeAttack = leeQuantity * (warriorLee + archerLee + riderLee);


        int warriorMing = 9;
        int archerMing = 35;
        int riderMing = 12;

        int mingAttack = mingQuantity * (warriorMing + archerMing + riderMing);

        System.out.printf("Lee`s army attack:  %d\nMing army attack:  %d", leeAttack, mingAttack);

    }
}
