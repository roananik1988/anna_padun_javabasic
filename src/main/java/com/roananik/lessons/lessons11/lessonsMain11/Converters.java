package com.roananik.lessons.lessons11.lessonsMain11;


public class Converters {
    public static void main(String[] args) {
        double celsiusValue = 20.0;

        ConvertersTemperature celsiusToKelvin = new KelvinCelsius();
        double kelvin = celsiusToKelvin.converterFromCelsius(celsiusValue);
        System.out.println(celsiusValue + "C" + " = " + kelvin + "K");
        double celsiusKelvin = celsiusToKelvin.converterToCelsius(kelvin);
        System.out.println(kelvin + "K" + " = " + celsiusKelvin + "C");

        ConvertersTemperature celsiusToFahrenheit = new FahrenheitCelsius();
        double fahrenheit = celsiusToFahrenheit.converterFromCelsius(celsiusValue);
        System.out.println(celsiusValue + "C" + " = " + fahrenheit + "F");
        double celsiusFahrenheit = celsiusToFahrenheit.converterToCelsius(fahrenheit);
        System.out.println(fahrenheit + "F" + " = " + celsiusFahrenheit + "C");


    }
}
