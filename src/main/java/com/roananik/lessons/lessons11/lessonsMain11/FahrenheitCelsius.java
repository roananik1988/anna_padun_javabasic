package com.roananik.lessons.lessons11.lessonsMain11;


public class FahrenheitCelsius extends ConvertersTemperature {
    public FahrenheitCelsius() {


    }

    @Override
    public double converterFromCelsius(double celsius) {
        return celsius * 1.8 + 32;
    }

    @Override
    public double converterToCelsius(double fahrenheit) {
        return (fahrenheit - 32) * 5 / 9;
    }
}
