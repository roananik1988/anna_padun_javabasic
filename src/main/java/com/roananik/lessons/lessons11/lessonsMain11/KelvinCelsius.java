package com.roananik.lessons.lessons11.lessonsMain11;

public class KelvinCelsius extends ConvertersTemperature {

    public KelvinCelsius() {

    }

    @Override
    public double converterToCelsius(double kelvin) {
        return kelvin - 273.15;
    }

    @Override
    public double converterFromCelsius(double celsius) {
        return celsius + 273.15;
    }
}
