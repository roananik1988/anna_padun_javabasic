package com.roananik.lessons.lessons16;

public class DoubleList implements MyList {
    private DoubleLinkedList head;
    private DoubleLinkedList tail;
    private static int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void addFirst(String value) {
        DoubleLinkedList element = new DoubleLinkedList(value);
        if (isEmpty()) {
            head = element;
            tail = element;
            element.next = null;
            element.prev = null;

        } else {
            element.next = head;
            element.prev = null;
            head.prev = element;
            head = element;

        }
        size++;

    }

    @Override
    public void addLast(String value) {
        DoubleLinkedList element = new DoubleLinkedList(value);
        if (isEmpty()) {
            addFirst(value);
            return;
        } else {
            element.next = null;
            element.prev = tail;
            tail.next = element;
            tail = element;
        }
        size++;

    }

    @Override
    public void add(int index, String value) {
        if (index == size) {
            addLast(value);
        } else if (index == 0) {
            addFirst(value);
        } else {
            validate(index);


            DoubleLinkedList current = head;

            for (int i = 0; i < index; i++) {
                current = current.next;

            }
            DoubleLinkedList previous = current.prev;


            DoubleLinkedList element = new DoubleLinkedList(value);
            element.next = current;
            element.prev = previous;
            previous.next = element;
            current.prev = element;


            size++;
        }


    }

    @Override
    public String get(int index) {
        validate(index);
        DoubleLinkedList list = head;
        for (int i = 0; i < index; i++) {
            list = list.next;
        }
        return list.value;
    }

    @Override
    public void set(int index, String value) {
        validate(index);
        DoubleLinkedList list = head;
        for (int i = 0; i < index; i++) {
            list = list.next;
        }
        list.value = value;

    }

    @Override
    public String remove(int index) {
        validate(index);


        if (index == 0) {
            return removeFirst();
        } else if (index >= size - 1) {
            return removeLast();
        } else {
            DoubleLinkedList previous = head;
            for (int i = 0; i < index; i++) {
                previous = previous.next;
            }


            DoubleLinkedList current = previous;
            current.prev.next = current.next;
            // крок назад два в перед
            current.next.prev = current.prev;
            //крок вперед два назад



            size--;

            return current.value;
        }
    }

    private String removeLast() {
        DoubleLinkedList last = tail;
        tail = last.prev;
        tail.next = null;
       //  last.prev =
        size--;

        return last.value;
    }

    private String removeFirst() {
        DoubleLinkedList list = head;
        head = head.next;
        head.prev = null;
        size--;

        return list.value;
    }

    @Override
    public String toString() {
        String str = "[";
        for (DoubleLinkedList list = head; list != null; list = list.next) {
            if (list != head) {
                str += ", ";
            }
            str += list.value;

        }
        str += "]";
        return str;
    }

    public void validate(int index) {
        if (index < 0 || index > size) {
            throw new MyListException("wrong index");
        }
        if (isEmpty()) {
            throw new MyListException("list is Empty");
        }
    }

    private static class DoubleLinkedList {
        private String value;
        private DoubleLinkedList next;
        private DoubleLinkedList prev;


        DoubleLinkedList(String value) {
            this.value = value;

        }

        DoubleLinkedList(String value, DoubleLinkedList next, DoubleLinkedList prev) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }


    }
}

