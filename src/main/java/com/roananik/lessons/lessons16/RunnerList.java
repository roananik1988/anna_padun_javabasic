package com.roananik.lessons.lessons16;

public class RunnerList {
    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.add(0, "1");
        list.addFirst("0");
        list.addLast("2");
        System.out.println(list);


        DoubleList liste= new DoubleList();
        liste.addFirst("rokot");
        liste.addFirst("pole");
        System.out.println(liste);
        liste.addLast("last");
        liste.add(2,"WIN");
        System.out.println(liste);
        System.out.println(liste.size());
        System.out.println(liste.get(2));
        liste.set(2,"NotWiN");
        System.out.println(liste);
       liste.remove(2);

        System.out.println(liste);
    }
}
