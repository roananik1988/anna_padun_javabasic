package com.roananik.lessons.lessons16;


public class MyListException extends RuntimeException {
    public MyListException(String message) {
        super(message);
    }
}

