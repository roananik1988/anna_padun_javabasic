package com.roananik.lessons.lessons07;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class TransposeMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the size of the array");
        System.out.println("heightMatrix: ");
        int heightMatrix = scanner.nextInt();

        System.out.println("widthMatrix: ");
        int widthMatrix = scanner.nextInt();

        int[][] firstMatrix = new int[heightMatrix][widthMatrix];
        fillMatrixRandomNumbers(firstMatrix);
        info(firstMatrix);

        System.out.println("...............");

        int[][] secondMatrix = fillMatrixWithNumberFromFirstMatrix(firstMatrix);
        info(secondMatrix);


    }

    public static void info(int[][] matrix) {
        for (int[] ints : matrix) {
            System.out.println(Arrays.toString(ints));
        }

    }

    public static int[][] fillMatrixWithNumberFromFirstMatrix(int[][] firstMatrix) {
        int[][] secondMatrix = new int[firstMatrix[0].length][firstMatrix.length];
        for (int i = 0; i < firstMatrix.length; i++) {
            for (int j = 0; j < firstMatrix[i].length; j++) {
                secondMatrix[j][i] = firstMatrix[i][j];
            }
        }
        return secondMatrix;
    }

    public static void fillMatrixRandomNumbers(int[][] matrix) {
        Random random = new Random();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = random.nextInt(50);
            }
        }
    }
}



