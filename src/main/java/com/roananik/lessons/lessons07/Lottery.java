package com.roananik.lessons.lessons07;

import java.util.Arrays;
import java.util.Random;

public class Lottery {
    static int amountOfNumbers = 7;
    static int randomNumbersLimit = 10;
    static boolean[] matchElement = new boolean[amountOfNumbers];


    public static void main(String[] args) {
        int[] lotteryOrganizer = new int[amountOfNumbers];
        int[] guessedNumbersByUser = new int[amountOfNumbers];

        fillsArrayRandomNumber(lotteryOrganizer, randomNumbersLimit);
        fillsArrayRandomNumber(guessedNumbersByUser, randomNumbersLimit);

        System.out.println(Arrays.toString(lotteryOrganizer));
        System.out.println(Arrays.toString(guessedNumbersByUser));
        System.out.println("......................");


        sortArray(lotteryOrganizer);
        sortArray(guessedNumbersByUser);

        System.out.println(Arrays.toString(lotteryOrganizer));
        System.out.println(Arrays.toString(guessedNumbersByUser));

        int numbersOfMatches = arraysEqualityComparison(lotteryOrganizer, guessedNumbersByUser);


        System.out.println("number of matches: " + numbersOfMatches);

        infoWonNumbers(lotteryOrganizer, guessedNumbersByUser);

    }

    public static void infoWonNumbers(int[] firstArray, int[] secondArray) {
        for (int i = 0; i < amountOfNumbers; i++) {
            if (firstArray[i] == secondArray[i]) {
                matchElement[i] = true;

                System.out.println("matched element: " + i);
            }
        }
    }

    public static int arraysEqualityComparison(int[] firstArray, int[] secondArray) {
        int count = 0;
        for (int i = 0; i < amountOfNumbers; i++) {
            if (firstArray[i] == secondArray[i]) {

                count++;
            }
        }
        return count;


    }

    public static void sortArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int minArrayIndex = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = minArrayIndex;
                }
            }

        }


    }

    public static void fillsArrayRandomNumber(int[] array, int range) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(range);
        }

    }

}
