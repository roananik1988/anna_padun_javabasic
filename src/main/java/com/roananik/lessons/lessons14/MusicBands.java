package com.roananik.lessons.lessons14;

public class MusicBands {
    public static void main(String[] args) {
        MusicStyles popBand = new PopMusic();
        MusicStyles rockBand = new RockMusic();
        MusicStyles classicBand = new ClassicMusic();

        MusicStyles[] play = {popBand, rockBand, classicBand};

        for (MusicStyles sound : play) {
            sound.playMusic();
        }
    }
}
