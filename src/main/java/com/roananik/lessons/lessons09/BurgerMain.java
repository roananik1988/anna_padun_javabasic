package com.roananik.lessons.lessons09;

public class BurgerMain {
    public static void main(String[] args) {
        Burger doubleBurger = new Burger("bun", "meat ", "cheese", "greens", "mayonnaise", "meat");
        Burger standardBurger = new Burger("bun", "meat", "cheese", "greens", "mayonnaise");
        Burger dietaryBurger = new Burger("bun", "meat", "cheese", "greens");
    }
}


class Burger {
    private final String bun;
    private final String meat;
    private final String cheese;
    private final String greens;
    private final String mayonnaise;


    public Burger(String bun, String meat, String cheese, String greens, String mayonnaise) {
        this.bun = bun;
        this.meat = meat;
        this.cheese = cheese;
        this.greens = greens;
        this.mayonnaise = mayonnaise;
        System.out.print("standard ");
        printInfo();
    }

    public Burger(String bun, String meat, String cheese, String greens) {
        this.bun = bun;
        this.meat = meat;
        this.cheese = cheese;
        this.greens = greens;
        this.mayonnaise = "without mayonnaise";
        System.out.print("dietary ");
        printInfo();
    }

    public Burger(String bun, String meat, String cheese, String greens, String mayonnaise, String additionalIngredient) {
        this.bun = bun;
        this.cheese = cheese;
        this.greens = greens;
        this.mayonnaise = mayonnaise;
        this.meat = meat + additionalIngredient;
        System.out.print("double ");
        printInfo();
    }

    private void printInfo() {
        System.out.printf("burger: %s, %s, %s, %s, %s. \n", bun, meat, cheese, greens, mayonnaise);
    }

}
