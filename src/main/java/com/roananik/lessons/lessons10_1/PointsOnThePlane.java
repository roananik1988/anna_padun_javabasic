package com.roananik.lessons.lessons10_1;

import java.util.Objects;

public class PointsOnThePlane {
    public static void main(String[] args) throws CloneNotSupportedException {
        Point firstPoint = new Point(3, 4);
        Point secondPoint = new Point(5, 8);

        System.out.println("First " + firstPoint);
        System.out.println("Second " + secondPoint);

        try {
            double distanceBetween = Point.distanceBetween(firstPoint, secondPoint);
            System.out.printf("distance between two points: %.2f \n", distanceBetween);

            double distanceTo = firstPoint.distanceTo(secondPoint);
            System.out.printf("distance between points: %.2f \n", distanceTo);

            System.out.println("EQUALS: " + firstPoint.equals(secondPoint));

            Point copyPoint = new Point(firstPoint);
            Point clonePoint = secondPoint.clone();

        } catch (NullPointerException e) {
            System.out.println("one of the points null");
        }

    }
}

class Point implements Cloneable {
    private int x;
    private int y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point other) {
        this.x = other.x;
        this.y = other.y;

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public Point clone() throws CloneNotSupportedException {
        return (Point) super.clone();
    }


    public double distanceTo(Point point) {
        if (point == null) {
            return -1;
        }
        int distanceX = this.x - point.x;
        int distanceY = this.y - point.y;
        return Math.sqrt(distanceX * distanceX + distanceY * distanceY);


    }

    public static double distanceBetween(Point first, Point second) {
        if (first == null || second == null) {

            return -1;
        }
        return first.distanceTo(second);
    }


    @Override
    public String toString() {
        return "Point coordinates:" +
                "x=" + x +
                ", y=" + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Point point = (Point) o;

        return x == point.x && y == point.y;
    }


    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}


