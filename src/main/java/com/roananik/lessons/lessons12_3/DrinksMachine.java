package com.roananik.lessons.lessons12_3;

import java.util.Scanner;

public class DrinksMachine {

    private double grandTotalPrice;
    private int totalDrinksCount;

    public double getGrandTotalPrice() {
        return grandTotalPrice;
    }

    public int getTotalDrinksCount() {
        return totalDrinksCount;
    }

    public void infoDrinks() {
        System.out.println("you can order...");
        for (Drinks drink : Drinks.values()) {
            System.out.println(drink + " price: " + drink.getPrice());
        }
        System.out.println("to select a drink enter its name\nwhen your done type \"exit\"");
    }

    public void coffeeMake() {
        System.out.println("preparing coffee....");
        totalDrinksCount++;
        grandTotalPrice += Drinks.values()[0].getPrice();

    }

    public void teeMake() {
        System.out.println("preparing tee....");
        totalDrinksCount++;
        grandTotalPrice += Drinks.values()[1].getPrice();
    }

    public void lemonadeMake() {
        System.out.println("preparing lemonade....");
        totalDrinksCount++;
        grandTotalPrice += Drinks.values()[2].getPrice();
    }

    public void mojitoMake() {
        System.out.println("preparing mojito....");
        totalDrinksCount++;
        grandTotalPrice += Drinks.values()[3].getPrice();
    }

    public void mineralWaterMake() {
        System.out.println("mineral water....");
        totalDrinksCount++;
        grandTotalPrice += Drinks.values()[4].getPrice();
    }

    public void colaMake() {
        System.out.println("coca cola....");
        totalDrinksCount++;
        grandTotalPrice += Drinks.values()[5].getPrice();
    }

    public void choiceOfDrink(Drinks drinks) {
        switch (drinks) {
            case COFFEE -> coffeeMake();
            case TEA -> teeMake();
            case LEMONADE -> lemonadeMake();
            case MOJITO -> mojitoMake();
            case WATER -> mineralWaterMake();
            case COLA -> colaMake();

        }

    }

    public void orderTable() {
        Scanner scanner = new Scanner(System.in);
        String drink;
        while (true) {
            drink = scanner.nextLine().toUpperCase();

            if (drink.equalsIgnoreCase("exit")) {
                break;
            }

            try {
                Drinks drinks = Drinks.valueOf(drink);
                choiceOfDrink(drinks);
            } catch (IllegalArgumentException e) {
                System.out.println("there is no such drink in the vending machine");
            }


        }
        scanner.close();
    }

    public void billInfo() {
        System.out.println("order price: " + getGrandTotalPrice());
        System.out.println("number of drinks ordered: " + getTotalDrinksCount());

    }

}
