package com.roananik.lessons.lessons12_3;

public enum Drinks {
    COFFEE(1.2),
    TEA(1.1),
    LEMONADE(2.1),
    MOJITO(4),
    WATER(0.5),
    COLA(2),
    ;

    private final double price;


    Drinks(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
