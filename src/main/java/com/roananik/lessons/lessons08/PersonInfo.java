package com.roananik.lessons.lessons08;



public class PersonInfo {

    public static void main(String[] args) {
        Person[] person = {new Person("Will", "Smith", "New York", "23459857463987"),
                new Person("Jackie", "Chan", "Shanghai", "5678944473"),
                new Person("Sherlock", "Holmes", "London", "88989544328")};


        for (Person persons : person) {
            persons.personInfo();
        }
    }
}

class Person {
    private String name;
    private String surname;
    private String city;
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCity() {
        return city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Person(String name, String surname, String city, String phoneNumber) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.phoneNumber = phoneNumber;
    }

    public void personInfo() {
        System.out.printf("call a citizen %s %s from the city %s you can be number %s \n", getName(), getSurname(),
                getCity(), getPhoneNumber());
    }

}