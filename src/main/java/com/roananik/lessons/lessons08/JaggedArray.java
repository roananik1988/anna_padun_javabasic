package com.roananik.lessons.lessons08;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class JaggedArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter the height of the array:");
        int height = scanner.nextInt();
        System.out.println("enter maximum length of the array:");
        int maximumLength = scanner.nextInt();

        int[][] jaggedArray = createArrayRandomNumbers(height, maximumLength);

        info(jaggedArray);
        System.out.println(".................");

        sortingPairedUnpairedRowsArray(jaggedArray);
        info(jaggedArray);
        System.out.println(".................");

        int sumElementsArray = sumElementsOfTwoDimensionalArray(jaggedArray);
        System.out.println("sum of all elements: " + sumElementsArray);

        int[] minimumValues = searchForMinimumValues(jaggedArray);
        System.out.println("minimum elements of jagged array: " + Arrays.toString(minimumValues));

        int absoluteMinimum = findMinimum(minimumValues);
        System.out.println("absolute minimum: " + absoluteMinimum);
        System.out.println("...................");
        System.out.println("division by the absolute minimum of all array elements");

        int[][] divisionResult = divideEachElementByNumber(jaggedArray, absoluteMinimum);
        impossibleInfo(absoluteMinimum);
        info(divisionResult);


    }

    public static void impossibleInfo(int number) {
        if (number == 0) {
            System.out.println("impossible!!! element = 0!!! result unchanged");
        }


    }

    public static int[][] divideEachElementByNumber(int[][] array, int number) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (number == 0) {

                    break;

                } else {
                    array[i][j] = array[i][j] / number;
                }
            }
        }
        return array;

    }

    public static void info(int[][] array) {
        for (int[] ints : array) {
            System.out.println(Arrays.toString(ints));
        }
    }

    public static int findMinimum(int[] array) {
        if (array.length == 0) {
            return 0;
        }
        int temp = array[0];
        for (int i = 1; i < array.length; i++) {
            if (temp > array[i]) {
                temp = array[i];
            }
        }
        return temp;

    }

    public static int[] searchForMinimumValues(int[][] array) {
        int[] minValues = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null || array[i].length == 0) {
                minValues[i] = 0;
                continue;
            }
            Arrays.sort(array[i]);
            minValues[i] = array[i][0];
        }
        return minValues;
    }

    public static int sumElementsOfTwoDimensionalArray(int[][] array) {
        int sum = 0;
        for (int[] row : array) {
            for (int element : row) {
                sum += element;
            }
        }
        return sum;
    }

    public static void reverseArray(int[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int temp = array[left];
            array[left] = array[right];
            array[right] = temp;
            left++;
            right--;
        }
    }

    public static void sortingPairedUnpairedRowsArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            Arrays.sort(array[i]);
            if (i % 2 != 0) {
                reverseArray(array[i]);
            }


        }
    }


    public static int[] createArray(int maximum) {
        return new int[maximum];

    }

    public static int[][] createArrayRandomNumbers(int height, int maximumLength) {
        Random random = new Random();
        int[][] array = new int[height][];
        for (int i = 0; i < array.length; i++) {
            array[i] = createArray(random.nextInt(maximumLength));
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(-10, 1000);
            }
        }

        return array;
    }
}
