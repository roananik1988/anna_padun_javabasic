package com.roananik.lessons.lessons06;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Rugby {
    public static void main(String[] args) {
        int[] vulturesTeam = new int[25];
        int[] elephantsTeam = new int[25];
        fillArrayWithRandomAge(vulturesTeam, 18, 40);
        fillArrayWithRandomAge(elephantsTeam, 18, 40);

        teamsInfo("Vulture", vulturesTeam, averageAgeCounts(vulturesTeam));

        teamsInfo("Elephant", elephantsTeam, averageAgeCounts(elephantsTeam));


    }

    public static void teamsInfo(String teamName, int[] array, double averageAge) {
        System.out.println("age of participants:  " + Arrays.toString(array));
        System.out.println("average age team " + teamName + ": " + averageAge);


    }


    public static void fillArrayWithRandomAge(int[] array, int minAge, int maxAge) {
        ThreadLocalRandom randomAge = ThreadLocalRandom.current();

        for (int i = 0; i < array.length; i++) {
            array[i] = randomAge.nextInt(minAge, maxAge + 1);

        }


    }

    public static double averageAgeCounts(int[] array) {
        int sum = 0;
        for (int j : array) {
            sum = sum + j;

        }
        return (double) sum / array.length;


    }

}
