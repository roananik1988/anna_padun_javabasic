package com.roananik.lessons.lessons02;

import java.util.Scanner;

public class Parallelepiped {
    public static void main(String[] args) {
        System.out.println("\tThis program calculates the volume of a \n\tparallelepiped and the " +
                "length of its sides");
        System.out.println("\tenter three parameters:\n    length, height, width IN CENTIMETERS");
        Scanner scanner = new Scanner(System.in);

        System.out.println("LENGTH");
        double topBottom = scanner.nextDouble();

        System.out.println("HEIGHT");
        double frontBack = scanner.nextDouble();

        System.out.println("WIDTH");
        double leftRight = scanner.nextDouble();

        scanner.close();


        double volume = topBottom * frontBack * leftRight;
        double length = 4 * (topBottom + frontBack + leftRight);


        System.out.printf("VOLUME Parallelepiped: %.2f\nLENGTH Parallelepiped: %.2f", volume, length);

    }
}
