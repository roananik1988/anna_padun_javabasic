package com.roananik.lessons.lessons02;

public class Coordinates {
    public static void main(String[] args) {
        System.out.println("now you will know the coordinates of the Nuremberg fortress");
        double latitude = 49.457967;
        double longitude = 11.075615;
        System.out.println("latitude: " + latitude + "\nlongitude: " + longitude);

    }
}
