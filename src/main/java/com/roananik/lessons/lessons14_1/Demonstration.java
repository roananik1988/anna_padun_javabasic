package com.roananik.lessons.lessons14_1;

import java.util.Scanner;

public class Demonstration {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        User.info();


        int attempt = 0;
        while (attempt < 3) {
            try {
                System.out.print("ENTER login: ");
                String login = scanner.nextLine();

                System.out.print("ENTER password: ");
                String password = scanner.nextLine();

                System.out.print("ENTER confirm password: ");
                String confirmPassword = scanner.nextLine();

                User user = new User(login, password, confirmPassword);

                System.out.println("created user with login: " + user.getLogin());
                break;

            } catch (WrongLoginException | WrongPasswordException e) {
                System.out.println("input Error: " + e.getMessage());
                attempt++;

            } finally {
                System.out.println("thanks for using our service");
            }


        }
    }
}
