package com.roananik.lessons.lessons14_1;

public class User {
    private final String login;
    private final String password;

    public User(String login, String password, String confirmPassword) throws WrongLoginException, WrongPasswordException {
        loginValidate(login);
        passwordValidate(password, confirmPassword);
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }


    private void loginValidate(String login) throws WrongLoginException {
        if (login == null || login.length() > 20 || !login.matches("^[a-zA-Z]+$")) {
            throw new WrongLoginException("login does not match");

        }
    }

    private void passwordValidate(String password, String confirmPassword) throws WrongPasswordException {
        if (password == null || password.length() < 6 ||
                password.length() > 25 || !password.matches("^(?=.*[a-zA-Z])(?=.*\\d).+$")) {
            throw new WrongPasswordException("password does not match");

        }
        if (!password.equals(confirmPassword)) {
            throw new WrongPasswordException("invalid password confirmation");

        }
    }

    public static void info() {
        System.out.println("""
                To register use latin letters.
                Login must be no more than 20 characters.
                Password must be at least 6 characters
                and contain a number and a special character""");
    }

}
