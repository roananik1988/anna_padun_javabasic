package com.roananik.lessons.lessons14_1;

public class WrongLoginException extends Exception {
    public WrongLoginException(String message) {
        super(message);
    }
}
