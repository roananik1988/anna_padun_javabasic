package com.roananik.lessons.lessons13;

public class Android implements Smartphones, LinuxOS {
    public Android() {
        linuxApp();
    }

    @Override
    public void linuxApp() {
        LinuxOS.super.linuxApp();
    }

    @Override
    public String call(String phoneNumber) {
        System.out.println("Android call....." + phoneNumber);
        return phoneNumber;
    }

    @Override
    public String sms(String text) {
        System.out.println("Android sending sms....." + text);
        return text;
    }

    @Override
    public String internet(String url) {
        System.out.println("Android browsing the internet....." + url);
        return url;
    }
}
