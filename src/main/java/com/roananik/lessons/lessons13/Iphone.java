package com.roananik.lessons.lessons13;

public class Iphone implements Smartphones, iOS {
    public Iphone() {
        iosApp();
    }

    @Override
    public void iosApp() {
        iOS.super.iosApp();
    }

    @Override
    public String call(String phoneNumber) {
        System.out.println("Iphone call...." + phoneNumber);
        return phoneNumber;
    }

    @Override
    public String sms(String text) {
        System.out.println("Iphone sending sms...." + text);
        return text;
    }

    @Override
    public String internet(String url) {
        System.out.println("Iphone browsing the internet...." + url);
        return url;
    }
}
