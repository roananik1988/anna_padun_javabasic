package com.roananik.lessons.lessons13;

public interface Smartphones {
    String call(String string);


    String sms(String string);

    String internet(String string);


}

interface LinuxOS {
    default void linuxApp() {
        System.out.println("system boot Linux");
    }

}

interface iOS {
    default void iosApp() {
        System.out.println("system boot IOS");

    }

}
