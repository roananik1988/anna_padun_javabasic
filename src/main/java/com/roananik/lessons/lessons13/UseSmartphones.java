package com.roananik.lessons.lessons13;

public class UseSmartphones {
    public static void main(String[] args) {
        Android android = new Android();
        android.call("44356798228");
        android.sms("\"hello\"");
        android.internet("https://lms.ithillel.ua");

        System.out.println("..........");

        Iphone iphone = new Iphone();
        iphone.call("4915209943429");
        iphone.sms("\"goodbye\"");
        iphone.internet("https://gitlab.com");

    }
}
