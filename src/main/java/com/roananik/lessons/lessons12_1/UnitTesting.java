package com.roananik.lessons.lessons12_1;

public class UnitTesting {
    public double average(int[] array) {
        if (array == null || array.length == 0) {
            return -1;

        }
        double sum = 0;

        for (int arrays : array) {
            sum += arrays;

        }
        return sum / array.length;
    }

    public boolean squareArray(int[][] array) {
        if ((array == null) || (array.length == 0)) {
            return false;

        }

        int arrayLength = array.length;
        for (int[] attachmentsArray : array) {
            if (attachmentsArray == null) {
                return false;
            }
            if (arrayLength != attachmentsArray.length) {

                return false;

            }

        }
        return true;
    }
}
