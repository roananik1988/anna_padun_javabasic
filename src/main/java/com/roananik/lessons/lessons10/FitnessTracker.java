package com.roananik.lessons.lessons10;



public class FitnessTracker {
    public static void main(String[] args) {
        Person annaPerson = new Person("Anna", "Verina", 20, 4,
                1998, "fart@gmail.com", "4988538256", 65.5, "120/90", 3000);
        annaPerson.printAccountInfo();

        Person lenaPerson = new Person("Lena", "Trokova", 14, 1,
                1985, "lena@gmail.com", "4988444444433", 55.2, "110/85", 3700);
        lenaPerson.printAccountInfo();

        Person mariaPerson = new Person("Maria", "Shalova", 8, 8,
                1978, "mariat@gmail.com", "38098538256", 70, "120/90", 7000);
        mariaPerson.printAccountInfo();

        lenaPerson.setWeight(54);
        lenaPerson.setStepsTaken(2000);
        mariaPerson.setPressure("130/90");
        mariaPerson.setStepsTaken(10000);

        lenaPerson.printAccountInfo();
        mariaPerson.printAccountInfo();

    }

}


class Person {
    private String firstName;
    private String lastName;
    private final int day;
    private final int month;
    private final int year;
    private final String email;
    private final String phone;
    private double weight;
    private String pressure;
    private int stepsTaken;
    private int age;
    private int thisYear = 2023;

    public int getAge() {
        return age;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public int getStepsTaken() {
        return stepsTaken;
    }

    public void setStepsTaken(int stepsTaken) {
        this.stepsTaken = stepsTaken;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Person(String firstName, String lastName, int day, int month, int year,
                  String email, String phone, double weight, String pressure, int stepsTaken) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.day = day;
        this.month = month;
        this.year = year;
        this.email = email;
        this.phone = phone;
        this.weight = weight;
        this.pressure = pressure;
        this.stepsTaken = stepsTaken;
        this.age = thisYear - year;
    }

    public void printAccountInfo() {
        System.out.printf("Username: %s %s\nDate of Birth: %02d/%02d/%d\nAge: %d\nE-mail:" +
                        " %s\nPhone Number: %s\nWeight: %.1f\nPressure: %s\nSteps Taken: %d\n..................\n",
                firstName, lastName, day, month, year, age, email, phone, weight, pressure, stepsTaken);
    }
}
