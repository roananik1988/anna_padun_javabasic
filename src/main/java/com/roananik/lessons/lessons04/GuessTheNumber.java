package com.roananik.lessons.lessons04;

import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int count = 3;
        System.out.println("guess the number from 0 to 10 from " + count + " times");

        int pcNumber = random.nextInt(11);
        do {

            int userNumber = scanner.nextInt();
            if (pcNumber == userNumber) {

                System.out.println("WIN!!!!");

                break;

            }

            count--;
            switch (count) {
                case 0 -> System.out.println("you lose");
                case 1 -> System.out.println("try once more");
                case 2 -> System.out.println("try again");
            }


        } while (count != 0);


        scanner.close();

    }
}
