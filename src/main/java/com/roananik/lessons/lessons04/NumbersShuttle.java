package com.roananik.lessons.lessons04;

import java.util.Arrays;

public class NumbersShuttle {
    public static boolean containsNumberInverted(int number, int target) {
        String stringUniqueNumber = String.valueOf(number);
        String excludeNumber = String.valueOf(target);
        return !stringUniqueNumber.contains(excludeNumber);
    }


    public static void main(String[] args) {
        int[] factory = new int[100];
        int uniqueNumber = 1;
        int arrayCount = 0;

        while (arrayCount != 100) {
            if (containsNumberInverted(uniqueNumber, 4) && containsNumberInverted(uniqueNumber, 9)) {
                factory[arrayCount] = uniqueNumber;
                arrayCount++;
            }
            uniqueNumber++;


        }
        System.out.println(Arrays.toString(factory));
    }
}
